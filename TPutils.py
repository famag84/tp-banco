#!/usr/bin/python3
#This is file : TPutils.py
import os, time, pickle, csv
import numpy as np
import pandas as pd

from sklearn import tree
from sklearn.linear_model import Perceptron
from sklearn.neural_network import MLPClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score
from sklearn.model_selection import KFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix

import matplotlib.pyplot as plt
from matplotlib.patches import Patch
cmap_data = plt.cm.Paired
cmap_cv = plt.cm.coolwarm

def kfold_Test(X, Y, model, Mconf='', h=True):
    model_path='models' 
    if not os.path.isdir(model_path): os.makedirs(model_path)

    nfolder = 0
    dictresu = {}
    kf = KFold(n_splits=10, shuffle=True, random_state=101)
    kf.get_n_splits(X)
    for train_index, test_index in kf.split(X):
        #print("TRAIN:", train_index, "TEST:", test_index)
        x_train, x_test = X.iloc[train_index], X.iloc[test_index]
        y_train, y_test = Y.iloc[train_index], Y.iloc[test_index].tolist()
    
        # Choose Model:
        if model == 'Tree': clf = tree.DecisionTreeClassifier()
        if model == 'Perceptron': clf = Perceptron(tol=1e-3, random_state=0) # Perceptron
        if model == 'MLPerceptron': clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1) # Multi-Layer Perceptron
        if model == 'NaiveBayes': clf = GaussianNB() # Naive Bayes
        if model == 'RandomForest': clf = RandomForestClassifier(n_estimator=10) # Random forest
            
        clf.fit(x_train, y_train) # Fitting the model
        
        fname = str(nfolder)+'_'+model+'.pkl'
        fname = os.path.join(model_path, fname)
        with open(fname, 'wb') as fid: pickle.dump(clf, fid)    

        y_pred = [clf.predict([x_test.iloc[irow].values])[0] for irow in range(len(test_index)) ] # Predicts the best set of atributes in the set of experiment
        dictresu['#Folder']=nfolder
        Mvalues = Mconf.get_confusionMatrix(y_test, y_pred)
        dictresu['tp'], dictresu['tn'], dictresu['fp'], dictresu['fn'] =Mvalues
        dictresu['CostoTotal'] = sum(Mconf.get_totalCostsMatrix())
        dictresu['accuracy'] = accuracy_score(y_test, y_pred)
        dictresu['model'] = model
        print('folder: ', nfolder, dictresu['accuracy'])
        with open('Modelos_'+time.strftime("%d%m%Y")+'.csv', 'a') as f:  # Just use 'w' mode in 3.x
            w = csv.DictWriter(f, dictresu.keys())
            if h: w.writeheader()
            w.writerow(dictresu)
            h = False
        nfolder +=1
    return 0 

class confusionMatrix:
    C = [1]*4
    #C = {'tp':1, 'tn':1, 'fp':1, 'fn':1}
    Cm = []
    def set_costsMatrix(Self, tpi=1,tni=1,fpi=1,fni=1):
        Self.C = [tpi, tni, fpi, fni]

    def get_confusionMatrix(Self, ydata, ypred):
        tp, tn, fp, fn = [0]*4 
        for i in range(len(ydata)):
            if ydata[i] == ypred[i]:
                if ypred[i]: tp+=1
                else: tn+=1
            if ydata[i] != ypred[i]:
                if ypred[i]: fp+=1
                else: fn+=1
        Self.Cm = [tp, tn, fp, fn]
        return(Self.Cm)

    def get_totalCostsMatrix(Self):
        return [Self.C[j]*Self.Cm[j] for j in range(4)]

# Examples from scikit learn 

#def plot_cv_indices(cv, X, y, group, ax, n_splits, lw=10):
def plot_cv_indices(cv, X, y, ax, n_splits, lw=10):
    """ Create a sample plot for indices of a cross-validation object.
    https://scikit-learn.org/stable/auto_examples/model_selection/plot_cv_indices.html#sphx-glr-auto-examples-model-selection-plot-cv-indices-py 
    """
    # Generate the training/testing visualizations for each CV split
    #for ii, (tr, tt) in enumerate(cv.split(X=X, y=y, groups=group)):
    for ii, (tr, tt) in enumerate(cv.split(X=X, y=y)):
        # Fill in indices with the training/test groups
        indices = np.array([np.nan] * len(X))
        indices[tt] = 1
        indices[tr] = 0

        # Visualize the results
        ax.scatter(range(len(indices)), [ii + .5] * len(indices),
                   c=indices, marker='_', lw=lw, cmap=cmap_cv,
                   vmin=-.2, vmax=1.2)

    # Plot the data classes and groups at the end
    ax.scatter(range(len(X)), [ii + 1.5] * len(X),
               c=y, marker='_', lw=lw, cmap=cmap_data)

#    ax.scatter(range(len(X)), [ii + 2.5] * len(X),
#               c=group, marker='_', lw=lw, cmap=cmap_data)

    # Formatting
    #yticklabels = list(range(n_splits)) + ['class', 'group']
    yticklabels = list(range(n_splits)) + ['class']
    ax.set(yticks=np.arange(n_splits+2) + .5, yticklabels=yticklabels,
           xlabel='Sample index', ylabel="CV iteration",
           ylim=[n_splits+2.2, -.2], xlim=[0, 100])
    ax.set_title('{}'.format(type(cv).__name__), fontsize=15)
    return ax

def plot_confusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    https://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()

def report2(results, n_top=3):
    '''
    https://scikit-learn.org/stable/auto_examples/model_selection/plot_randomized_search.html#sphx-glr-auto-examples-model-selection-plot-randomized-search-py
    '''
    # Utility function to report best scores
    df = pd.DataFrame() 
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results['rank_test_score'] == i)
        myd = {}
        for candidate in candidates:
            myd['Rank'] = i 
            myd['MeanTestScore'] = results['mean_test_score'][candidate]
            myd['StdTestScore'] = results['std_test_score'][candidate]
            myd.update(results['params'][candidate])      
        df = df.append(myd, ignore_index=True)
    return df

def report(results, n_top=3):
    '''
    https://scikit-learn.org/stable/auto_examples/model_selection/plot_randomized_search.html#sphx-glr-auto-examples-model-selection-plot-randomized-search-py
    '''
    # Utility function to report best scores
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results['rank_test_score'] == i)
        for candidate in candidates:
            print("Model with rank: {0}".format(i))
            print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
                  results['mean_test_score'][candidate],
                  results['std_test_score'][candidate]))
            print("Parameters: {0}".format(results['params'][candidate]))
            print("")

# Built my own scorer function: Gain

def tn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 0]
def fp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 1]
def fn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 0]
def tp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 1]

def ganancia(y_true, y_pred): 
        return 850*tp(y_true, y_pred)-750*fn(y_true, y_pred)-100*fp(y_true, y_pred)

def cat_to_dummies(var_categoricas, dict_cate, df):
    databin = pd.DataFrame()
       
    for cat in var_categoricas:
    #https://stackoverflow.com/questions/32387266/converting-categorical-values-to-binary-using-pandas#32389185
        daux = pd.get_dummies(df[cat])
        lname = daux.columns
        databin = pd.concat([databin, daux[dict_cate[cat]]], axis=1)

    return databin

def preproDataSet(df):
    # Convierto algunas variables categoricas a numericas
    df['y'].replace({'si': 1, 'no': 0}, inplace=True)
    df['deuda'].replace({'si': 1, 'no': 0}, inplace=True)
    df['vivienda'].replace({'si': 1, 'no': 0}, inplace=True)
    df['prestamo'].replace({'si': 1, 'no': 0}, inplace=True)
    df['mes'] = df['mes'].map({"ene":1, "feb":2, "mar":3, "abr":4, "may":5, "jun":6, 
                                      "jul":7, "ago":8, "sep":9, "oct":10, "nov":11, "dic":12,})

    # Creacion de nuevas variables 
    df['periodo'] = df['anio']*100+df['mes']
       
    df.loc[df.diasentre >= 0, 'bdiasentre'] = 1
    df.loc[df.diasentre < 0, 'bdiasentre'] = 0
    df['diasentre'].replace(-1,0, inplace=True)
       
    # Categoricas to dummies
    lista_categoricas = ['categlaboral', 'estadocivil', 'niveleducacion', 'contacto', 'resultadoant',]

    dict_categoricas = {'categlaboral': ['servicio domestico', 'tecnico', 'desempleado', 'gestion', 'servicios', 'independiente', 'operario', 'estudiante', 'empresario', 'jubilado', 'administrador'], #'desconocido'
                        'estadocivil': ['soltero', 'casado'], # Separado/Viudo
                        'niveleducacion': ['primario', 'secundario', 'universitario/terciario'], #'desconocido'
                        'contacto': ['celular', 'telefono fijo'] , #'desconocido'
                        'resultadoant': ['otro', 'fallo', 'exito'], #'desconocido'
                        }
       
    colnames =df.columns.values
    ddummies = cat_to_dummies(lista_categoricas, dict_categoricas, df)
    df = pd.concat([df, ddummies], axis=1)
                
    features = [i for i in colnames if i not in dict_categoricas ] 
                        
    for k in dict_categoricas.keys():
        features = features + dict_categoricas[k] 
                           
    return df[features]

def predict_with_cutoff(colname, y_prob, df):
    n_events = df[colname].values
    event_rate = sum(n_events) / float(df.shape[0]) * 100
    threshold = np.percentile(y_prob[:, 1], 100 - event_rate)
    print("Cutoff/threshold at: " + str(threshold))
    y_pred = [1 if x >= threshold else 0 for x in y_prob[:, 1]]
    
    return y_pred

# Here we will choose the variables to be used by the RandomForest model.
def define_variables(df): 
    #var_temporales = ['dia', 'mes', 'anio' 
    #var_continuas = ['edad', 'balance', 'duracion', 'cantactual', 'cantprevia', 'diasentre',  ]
    #var_binarias = ['deuda', 'vivienda', 'prestamo', 'bdiasentre']
    #var_categoricas = ['categlaboral', 'estadocivil', 'niveleducacion', 'contacto', 'resultadoant', ]
                 
    target = 'y' # This is the dataset of class/clasification of each experiment
    # features are in the dataset of attributes of each experiment
    #features = var_binarias + var_continuas + var_temporales #+ var_categoricas
    features = [k for k in df.columns if k != 'y']
    return (df[features], df[target])

def feature_importances(clf, X, q=0.05):
    importances = clf.feature_importances_
    
    std = np.std([tree.feature_importances_ for tree in best_clf.estimators_], axis=0)
    
    indices = np.argsort(importances)[::-1]
        
    return {Xdata.columns[f]: importances[indices[f]] for f in range(X.shape[1]) if importances[indices[f]] > q }

def build_classifier(name):
    if name.lower() == "randomforest":
        clf = RandomForestClassifier(n_estimators=20, random_state=101,
                criterion = "entropy",
                n_jobs=2, 
                #verbose=0, 
                #warm_start=False, 
                #class_weight=None,
                #min_weight_fraction_leaf=0.0, 
                #max_features='auto', 
                #max_leaf_nodes=None, 
                #min_impurity_decrease=0.0, 
                #min_impurity_split=None,  
                #oob_score=False,
                )
        
        # specify parameters and distributions to sample from
        param_dist = {"criterion": ["gini", "entropy"],
                "max_depth": [17, None],
                "max_features": sp_randint(1, 35), # cambiar estos valores!
                "min_samples_split": sp_randint(2, 9), # creo que deberia haber al menos 17
                "bootstrap": [True, False], # No se que hace esta variable
                }

    return [clf, param_dist]


def my_cutoff(coff, Yprob):
    newYpred = [1 if x >= coff else 0 for x in Yprob[:,1]]
    return newYpred

def get_best_scenarios(dr, n_scenarios):
    lista = []
    params = list(dr.columns[3:])

    for n in range(n_scenarios):
        try: 
            dict_scenario = {k:int(dr[k][n]) for k in params}
            dict_scenario['n_estimators'] = 20
            dict_scenario['random_state'] = 101
            dict_scenario['criterion'] = 'entropy'
            lista.append(dict_scenario)
        except Exception: continue            
    
    return lista
