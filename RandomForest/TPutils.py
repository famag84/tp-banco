#!/usr/bin/python3
#This is file : TPutils.py
import os, time, pickle, csv
#import cPickle as pickle

from joblib import dump, load
import numpy as np
import pandas as pd
import itertools

from sklearn import tree
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import make_scorer
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split
from sklearn.model_selection import RandomizedSearchCV

from scipy.stats import randint as sp_randint
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC

import matplotlib.pyplot as plt
from matplotlib.patches import Patch

cmap_data = plt.cm.Paired
cmap_cv = plt.cm.coolwarm

def kfold_Test(X, Y, model, Mconf='', h=True):
    model_path='models' 
    if not os.path.isdir(model_path): os.makedirs(model_path)

    nfolder = 0
    dictresu = {}
    kf = KFold(n_splits=10, shuffle=True, random_state=101)
    kf.get_n_splits(X)
    for train_index, test_index in kf.split(X):
        #print("TRAIN:", train_index, "TEST:", test_index)
        x_train, x_test = X.iloc[train_index], X.iloc[test_index]
        y_train, y_test = Y.iloc[train_index], Y.iloc[test_index].tolist()
    
        # Choose Model:
        if model == 'Tree': clf = tree.DecisionTreeClassifier()
        if model == 'Perceptron': clf = Perceptron(tol=1e-3, random_state=0) # Perceptron
        if model == 'MLPerceptron': clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1) # Multi-Layer Perceptron
        if model == 'NaiveBayes': clf = GaussianNB() # Naive Bayes
        if model == 'RandomForest': clf = RandomForestClassifier(n_estimator=10) # Random forest
            
        clf.fit(x_train, y_train) # Fitting the model
        
        fname = str(nfolder)+'_'+model+'.pkl'
        fname = os.path.join(model_path, fname)
        with open(fname, 'wb') as fid: pickle.dump(clf, fid)    

        y_pred = [clf.predict([x_test.iloc[irow].values])[0] for irow in range(len(test_index)) ] # Predicts the best set of atributes in the set of experiment
        dictresu['#Folder']=nfolder
        Mvalues = Mconf.get_confusionMatrix(y_test, y_pred)
        dictresu['tp'], dictresu['tn'], dictresu['fp'], dictresu['fn'] =Mvalues
        dictresu['CostoTotal'] = sum(Mconf.get_totalCostsMatrix())
        dictresu['accuracy'] = accuracy_score(y_test, y_pred)
        dictresu['model'] = model
        print('folder: ', nfolder, dictresu['accuracy'])
        with open('Modelos_'+time.strftime("%d%m%Y")+'.csv', 'a') as f:  # Just use 'w' mode in 3.x
            w = csv.DictWriter(f, dictresu.keys())
            if h: w.writeheader()
            w.writerow(dictresu)
            h = False
        nfolder +=1
    return 0 

def save_graph(x,y, title='mytitle', xl='xLabel', yl='yLabel', fname='aux.png'):
    import matplotlib.pyplot as plt
    fig = plt.figure()
    
    plt.title(title )
    plt.xlabel(xl)
    plt.ylabel(yl)
    plt.plot(x, y, 'g.')
    
    fig.savefig(fname)
    return 0 

class confusionMatrix:
    C = [1]*4
    #C = {'tp':1, 'tn':1, 'fp':1, 'fn':1}
    Cm = []
    def set_costsMatrix(Self, tpi=1,tni=1,fpi=1,fni=1):
        Self.C = [tpi, tni, fpi, fni]

    def get_confusionMatrix(Self, ydata, ypred):
        tp, tn, fp, fn = [0]*4 
        for i in range(len(ydata)):
            if ydata[i] == ypred[i]:
                if ypred[i]: tp+=1
                else: tn+=1
            if ydata[i] != ypred[i]:
                if ypred[i]: fp+=1
                else: fn+=1
        Self.Cm = [tp, tn, fp, fn]
        return(Self.Cm)

    def get_totalCostsMatrix(Self):
        return [Self.C[j]*Self.Cm[j] for j in range(4)]

# Examples from scikit learn 

#def plot_cv_indices(cv, X, y, group, ax, n_splits, lw=10):
def plot_cv_indices(cv, X, y, ax, n_splits, lw=10):
    """ Create a sample plot for indices of a cross-validation object.
    https://scikit-learn.org/stable/auto_examples/model_selection/plot_cv_indices.html#sphx-glr-auto-examples-model-selection-plot-cv-indices-py 
    """
    # Generate the training/testing visualizations for each CV split
    #for ii, (tr, tt) in enumerate(cv.split(X=X, y=y, groups=group)):
    for ii, (tr, tt) in enumerate(cv.split(X=X, y=y)):
        # Fill in indices with the training/test groups
        indices = np.array([np.nan] * len(X))
        indices[tt] = 1
        indices[tr] = 0

        # Visualize the results
        ax.scatter(range(len(indices)), [ii + .5] * len(indices),
                   c=indices, marker='_', lw=lw, cmap=cmap_cv,
                   vmin=-.2, vmax=1.2)

    # Plot the data classes and groups at the end
    ax.scatter(range(len(X)), [ii + 1.5] * len(X),
               c=y, marker='_', lw=lw, cmap=cmap_data)

#    ax.scatter(range(len(X)), [ii + 2.5] * len(X),
#               c=group, marker='_', lw=lw, cmap=cmap_data)

    # Formatting
    #yticklabels = list(range(n_splits)) + ['class', 'group']
    yticklabels = list(range(n_splits)) + ['class']
    ax.set(yticks=np.arange(n_splits+2) + .5, yticklabels=yticklabels,
           xlabel='Sample index', ylabel="CV iteration",
           ylim=[n_splits+2.2, -.2], xlim=[0, 100])
    ax.set_title('{}'.format(type(cv).__name__), fontsize=15)
    return ax

def plot_confusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    https://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    #print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()

def report2(clf_name, results,Dt,n_iter, n_top=3):
    ''' source:  https://scikit-learn.org/stable/auto_examples/model_selection/plot_randomized_search.html#sphx-glr-auto-examples-model-selection-plot-randomized-search-py
    Build a report, given the results from the optimizer
    input: 
        clasifier name, results, 
        Dt: Time used by the optimizer (random_search(?))
        n_iter: number of candidate generated
        n_top: the number of models to be saved. the best "n_top" models will be saved.
    output: 
        df : a dataframe with all the parameters of the "n_top" models.
    '''

    # Utility function to report best scores
    df = pd.DataFrame() 
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results['rank_test_score'] == i)
        myd = {}
        for candidate in candidates:
            myd['Rank'] = i 
            myd['MeanTestScore'] = results['mean_test_score'][candidate]
            myd['StdTestScore'] = results['std_test_score'][candidate]
            myd.update(results['params'][candidate])      

        daux = pd.DataFrame.from_dict([myd])
        daux['Dt[s]'] = Dt
        daux['nCandidates'] = n_iter
        daux.to_csv(clf_name +'.log', mode='a', index=False, encoding='utf-8', header=True )

        df = df.append(myd, ignore_index=True)
    
    return df

def report(results, n_top=3):
    '''
    https://scikit-learn.org/stable/auto_examples/model_selection/plot_randomized_search.html#sphx-glr-auto-examples-model-selection-plot-randomized-search-py
    '''
    # Utility function to report best scores
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results['rank_test_score'] == i)
        for candidate in candidates:
            print("Model with rank: {0}".format(i))
            print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
                  results['mean_test_score'][candidate],
                  results['std_test_score'][candidate]))
            print("Parameters: {0}".format(results['params'][candidate]))
            print("")

# Built my own scorer function: Gain

def tn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 0]
def fp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 1]
def fn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 0]
def tp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 1]

def ganancia(y_true, y_pred): 
    ''' This is the function to be optimized by the random_serach method. 
        (Scoring function)
        input: list of true target values (1st argument) and list of predicted target values (2nd arg)
        output: net profit. Float or real value.
    '''
    #return 850*tp(y_true, y_pred)-750*fn(y_true, y_pred)-100*fp(y_true, y_pred)
    return 750*tp(y_true, y_pred)-750*fn(y_true, y_pred)-100*fp(y_true, y_pred)

def random_search(clf_name, clf, param_dist, Xdata, ydata, n_iter_search=10):
    random_search = RandomizedSearchCV(clf, n_iter = n_iter_search, 
                                   param_distributions = param_dist,
                                   scoring = make_scorer(ganancia),
                                   #refit = 'recall',
                                   n_jobs = -1, 
                                   iid = False, cv=6)

    start = time.time()
    random_search.fit(Xdata, ydata)
    end = time.time()
    
    results = random_search.cv_results_
    
    print("RandomizedSearchCV took %.2f seconds for %d candidates"
          " parameter settings." % ((end - start), n_iter_search))
  
    return [results, end-start, n_iter_search]
   
def cat_to_dummies(var_categoricas, dict_cate, df):
    databin = pd.DataFrame()
       
    for cat in var_categoricas:
    #https://stackoverflow.com/questions/32387266/converting-categorical-values-to-binary-using-pandas#32389185
        daux = pd.get_dummies(df[cat])
        lname = daux.columns
        databin = pd.concat([databin, daux[dict_cate[cat]]], axis=1)

    return databin

def preproDataSet(df):
    # Convierto algunas variables categoricas a numericas
    df['y'].replace({'si': 1, 'no': 0}, inplace=True)
    df['deuda'].replace({'si': 1, 'no': 0}, inplace=True)
    df['vivienda'].replace({'si': 1, 'no': 0}, inplace=True)
    df['prestamo'].replace({'si': 1, 'no': 0}, inplace=True)
    df['mes'] = df['mes'].map({"ene":1, "feb":2, "mar":3, "abr":4, "may":5, "jun":6, 
                                      "jul":7, "ago":8, "sep":9, "oct":10, "nov":11, "dic":12,})

    # Creacion de nuevas variables 
    df['periodo'] = df['anio']*100+df['mes']
       
    df.loc[df.diasentre >= 0, 'bdiasentre'] = 1
    df.loc[df.diasentre < 0, 'bdiasentre'] = 0
    df['diasentre'].replace(-1,0, inplace=True)
       
    # Categoricas to dummies
    lista_categoricas = ['categlaboral', 'estadocivil', 'niveleducacion', 'contacto', 'resultadoant',]

    dict_categoricas = {'categlaboral': ['servicio domestico', 'tecnico', 'desempleado', 'gestion', 'servicios', 'independiente', 'operario', 'estudiante', 'empresario', 'jubilado', 'administrador'], #'desconocido'
                        'estadocivil': ['soltero', 'casado'], # Separado/Viudo
                        'niveleducacion': ['primario', 'secundario', 'universitario/terciario'], #'desconocido'
                        'contacto': ['celular', 'telefono fijo'] , #'desconocido'
                        'resultadoant': ['otro', 'fallo', 'exito'], #'desconocido'
                        }
       
    colnames =df.columns.values
    ddummies = cat_to_dummies(lista_categoricas, dict_categoricas, df)
    df = pd.concat([df, ddummies], axis=1)
                
    features = [i for i in colnames if i not in dict_categoricas ] 
                        
    for k in dict_categoricas.keys():
        features = features + dict_categoricas[k] 
                           
    return df[features]

def test_preproDataSet(df):
    # Convierto algunas variables categoricas a numericas
    #df['y'].replace({'si': 1, 'no': 0}, inplace=True)
    df['deuda'].replace({'si': 1, 'no': 0}, inplace=True)
    df['vivienda'].replace({'si': 1, 'no': 0}, inplace=True)
    df['prestamo'].replace({'si': 1, 'no': 0}, inplace=True)
    df['mes'] = df['mes'].map({"ene":1, "feb":2, "mar":3, "abr":4, "may":5, "jun":6, 
                                      "jul":7, "ago":8, "sep":9, "oct":10, "nov":11, "dic":12,})

    # Creacion de nuevas variables 
    df['periodo'] = df['anio']*100+df['mes']
       
    df.loc[df.diasentre >= 0, 'bdiasentre'] = 1
    df.loc[df.diasentre < 0, 'bdiasentre'] = 0
    df['diasentre'].replace(-1,0, inplace=True)
       
    # Categoricas to dummies
    lista_categoricas = ['categlaboral', 'estadocivil', 'niveleducacion', 'contacto', 'resultadoant',]

    dict_categoricas = {'categlaboral': ['servicio domestico', 'tecnico', 'desempleado', 'gestion', 'servicios', 'independiente', 'operario', 'estudiante', 'empresario', 'jubilado', 'administrador'], #'desconocido'
                        'estadocivil': ['soltero', 'casado'], # Separado/Viudo
                        'niveleducacion': ['primario', 'secundario', 'universitario/terciario'], #'desconocido'
                        'contacto': ['celular', 'telefono fijo'] , #'desconocido'
                        'resultadoant': ['otro', 'fallo', 'exito'], #'desconocido'
                        }
       
    colnames =df.columns.values
    ddummies = cat_to_dummies(lista_categoricas, dict_categoricas, df)
    df = pd.concat([df, ddummies], axis=1)
                
    features = [i for i in colnames if i not in dict_categoricas ] 
                        
    for k in dict_categoricas.keys():
        features = features + dict_categoricas[k] 
                           
    return df[features]

def predict_with_cutoff(colname, y_prob, df):
    n_events = df[colname].values
    event_rate = sum(n_events) / float(df.shape[0]) * 100
    threshold = np.percentile(y_prob[:, 1], 100 - event_rate)
    print("Cutoff/threshold at: " + str(threshold))
    y_pred = [1 if x >= threshold else 0 for x in y_prob[:, 1]]
    
    return y_pred

# Here we will choose the variables to be used by the RandomForest model.
def define_variables(df): 
    #var_temporales = ['dia', 'mes', 'anio' 
    #var_continuas = ['edad', 'balance', 'duracion', 'cantactual', 'cantprevia', 'diasentre',  ]
    #var_binarias = ['deuda', 'vivienda', 'prestamo', 'bdiasentre']
    #var_categoricas = ['categlaboral', 'estadocivil', 'niveleducacion', 'contacto', 'resultadoant', ]
                 
    target = 'y' # This is the dataset of class/clasification of each experiment
    # features are in the dataset of attributes of each experiment
    #features = var_binarias + var_continuas + var_temporales #+ var_categoricas
    features = [k for k in df.columns if k != 'y']
    return (df[features], df[target])

def feature_importances(clf, X, q=0.05, ts=999):
    importances = clf.feature_importances_
    
    std = np.std([tree.feature_importances_ for tree in clf.estimators_], axis=0)
    
    indices = np.argsort(importances)[::-1]
    
    #fdict = {'feature_'+X.columns[f]: importances[indices[f]] for f in range(X.shape[1]) if importances[indices[f]] > q }
    daux = pd.DataFrame({'feature': [X.columns[f] for f in range(X.shape[1]) if importances[indices[f]] > q], 
             'importance': [importances[indices[f]] for f in range(X.shape[1]) if importances[indices[f]] > q] })

#    daux = pd.DataFrame.from_dict(fdict)
    daux['timestamp'] = ts
    daux.to_csv('feature_importances.csv', mode='a', index=False, )

    return 0  

def build_classifier(clf_name):
    name = clf_name.lower()
    if  name == "randomforest":
        default_dict = {'n_estimators': 20,
                        'random_state': 101, 
                        'n_jobs': -1,
                        'criterion': "entropy",
                        #verbose=0, 
                        #warm_start=False, 
                        #class_weight=None,
                        #min_weight_fraction_leaf=0.0, 
                        #max_features='auto', 
                        #max_leaf_nodes=None, 
                        #min_impurity_decrease=0.0, 
                        #min_impurity_split=None,  
                        #oob_score=False,
                       }
        clf = RandomForestClassifier( **default_dict )
        
        # specify parameters and distributions to sample from
        param_dist = {#"criterion": ["gini", "entropy"],
                "max_depth": [17, None],
                "max_features": sp_randint(1, 35), # cambiar estos valores!
                "min_samples_split": sp_randint(2, 20), # creo que deberia haber al menos 17
                "bootstrap": [True, False], # No se que hace esta variable
                }
    elif name == 'svm':
        default_dict = {'gamma': 'auto', 
                        'verbose': False,
                        'decision_function_shape': 'ovr', 
                        'random_state': 101, 
                       }
        clf = SVC( **default_dict )
        
        # specify parameters and distributions to sample from
        param_dist = {'C': scipy.stats.expon(scale=100), #[1.0,], # 1.0
                      'kernel': ['linear', 'poly', 'rbf', 'sigmoid'], # 'rbf'
                      'degree':[2, 3], # Only used when 'poly' in kernel
                      'gamma': ['scale', 'auto'], # 'auto'
                      'coef0': [0.0], # It is only significant in ‘poly’ and ‘sigmoid’.
                      'shrinking': [False, True], # Default True
                      'probability': [False], # Whether to enable probability estimates. This must be enabled prior to calling fit, and will slow down that method.
                      'tol': [0.001], # (default=1e-3)
                      'cache_size': 200, # Specify the size of the kernel cache (in MB).
                      'class_weight': ['balanced', None], # Se puede usar 'balanced' mode (toma en cuenta la frecuencia de las clases en target)               
                      'max_iter': [-1], # Hard limit on iterations within solver, or -1 for no limit.
                }
    
    return [clf, param_dist, default_dict]

def build_classifier2(clf_name, default_dict):
    name = clf_name.lower()
    if  name == "randomforest":
        clf = RandomForestClassifier( **default_dict )
        
    elif name == 'svm':
        clf = SVC( **default_dict )
        
    return clf

def my_cutoff(coff, Yprob):
    newYpred = [1 if x >= coff else 0 for x in Yprob[:,1]]
    return newYpred

def get_best_scenarios(dr, n_scenarios, default_dict):
    lista = []
    params = list(dr.columns[3:])
    len_dr = dr.shape[0]
    n_s = 0
    for n in range(len_dr):
        try: 
            dict_scenario = {k:(int(dr[k][n]) if type(dr[k][0]) == type(np.float64()) else dr[k][n]) for k in params  }
            #dict_scenario = {k:dr[k][n] for k in params}
            
            for k in default_dict.keys(): dict_scenario[k] = default_dict[k]
                
            lista.append(dict_scenario)
            n_s+=1
            if (n_s >= n_scenarios): break 
        except Exception as e: 
            print('error found in scenario: ', n, 'error: ', e)
            continue            
    
    return lista

def choose_best_cutoff(ytest, y_prob):
    xlist = np.linspace(0,0.5,50)
    y = []
    for x in xlist:
        new_y_pred = [1 if yp >= x else 0 for yp in y_prob[:,1]]
        y.append(ganancia(ytest, new_y_pred))

    ind = np.argmax(y)

    return [xlist[ind], y[ind]]

def testing_scenarios(my_scenarios, clf_name, Xdata, ydata, Xtest, ytest):
    df_output = pd.DataFrame()
    dict_output = {}

    for ns, dict_s in enumerate(my_scenarios):

        timestamp = int(time.time())
        model_filename = 'Trained_'+ clf_name+'_'+str(ns)+'_'+str(timestamp)+'.joblib'
        
        best_clf = RandomForestClassifier(**dict_s)
        best_clf.fit(Xdata, ydata)

#        y_pred = best_clf.predict(Xtest)
        y_prob = best_clf.predict_proba(Xtest)

        q , Ga = choose_best_cutoff(ytest, y_prob)
        #new_y_pred = my_cutoff(q, y_prob)

        dict_output['cutoff'] = q
        dict_output['Ga'] = Ga
        
        dict_output['timestamp'] = timestamp
        dict_output['zfilename'] = model_filename
        dict_output['Gs'] = ganancia(ytest, [1 for k in range(len(ytest))])
        #dict_output['Ga'] = ganancia(ytest, new_y_pred)
        dict_output['Gmax'] = ganancia(ytest, ytest)
        dict_output['rank'] = ns
#        dict_output['zDictFeature'] = dict_features
        feature_importances(best_clf, Xdata, q=0.05, ts=timestamp)

        dict_s = dict(('scenario_'+key, value) for (key, value) in dict_s.items())
        dict_output = {**dict_output, **dict_s}
#        dict_output['zDictScenario'] = dict_s

        #Guardar Modelo con ?
        dump(best_clf, model_filename) 

        df_output = df_output.append(dict_output, ignore_index=True)

        print('Ganancia Si a todo: ', dict_output['Gs'], 
              #'\nGanancia default: ', dict_output['Gd'], 
              '\nGanancia con my_cutoff: ', dict_output['Ga'])
    
    return df_output

def save_ganancia_vs_p(ytest, y_prob, title):
    xlist = np.linspace(0.001,1,50)
    y = []
    for x in xlist:
        new_y_pred = my_cutoff(x, y_prob) # new_y_pred = [1 if x >= coff else 0 for x in y_prob[:,1]]
        y.append( ganancia(ytest, new_y_pred))

    fig = plt.figure()
    
    xl='Punto de Corte'
    yl='Ganancia' 
    fname=title+'.png'
    
    xcoords = [1/17, 1/16, 0.5]
    colors = ['r','b','k']
    
    for xc,c in zip(xcoords,colors):
        plt.axvline(x=xc, label='p = {0:.4f}'.format(xc), c=c)
 
    plt.legend()
    
    plt.title(title )
    plt.xlabel(xl)
    plt.ylabel(yl)
    plt.plot(xlist, y, 'g.')
    fig.savefig(fname, dpi='300')
    #save_graph(xlist, y, title=title, )


def test_scenario(my_scenario, clf_name, Xdata, ydata, Xtest, ytest, label='default'):
    df_output = pd.DataFrame()
    dict_output = {}

    best_clf = RandomForestClassifier(**dict_s)
    best_clf.fit(Xdata, ydata)

    y_pred = best_clf.predict(Xtest)
    y_prob = best_clf.predict_proba(Xtest)

    new_y_pred = my_cutoff(1/17, y_prob) # new_y_pred = [1 if x >= coff else 0 for x in y_prob[:,1]]
   
    save_ganancia_vs_p(ytest, y_prob, clf_name+"_"+str(label) )
    
    timestamp = int(time.time())
    model_filename = 'Trained_'+ clf_name+'_'+str(ns)+'_'+str(timestamp)+'.joblib'
    

    dict_output['timestamp'] = timestamp
    dict_output['zfilename'] = model_filename
    dict_output['Gs'] = ganancia(ytest, [1 for k in range(len(ytest))])
    dict_output['Ga'] = ganancia(ytest, new_y_pred)
    dict_output['Gmax'] = ganancia(ytest, ytest)
    dict_output['rank'] = ns
#    dict_output['zDictFeature'] = dict_features
    feature_importances(best_clf, Xdata, q=0.05, ts=timestamp)

    dict_s = dict(('scenario_'+key, value) for (key, value) in dict_s.items())
    dict_output = {**dict_output, **dict_s}
#    dict_output['zDictScenario'] = dict_s

    #Guardar Modelo con ?
    dump(best_clf, model_filename) 

    df_output = df_output.append(dict_output, ignore_index=True)

    print('Ganancia Si a todo: ', dict_output['Gs'], 
          #'\nGanancia default: ', dict_output['Gd'], 
          '\nGanancia con my_cutoff: ', dict_output['Ga'])
    
    return df_output


def prepare_data(df, tsize, rnd_state=101):
    '''
    # Prepare Test data and Training data
    input:
        df : dataframe with all the training data features + target
        tsize : fraction of the original data to be used in Testing fase. (taken randomly)
        rnd_state: define de seed for random number
    output: It is a list [fdata, ydata, ftest, ytest]
        fdata, ydata : means feature and target data to be used in training fase.
        ftest, ytest : means feature and target data to be used in testing fase.
    '''
    #X, Y = define_variables(df)
    features = [k for k in df.columns if k != 'y']
    X = df[features]
    Y = df['y']

    # Separo data para Testing
    Xdata, Xtest, ydata, ytest = train_test_split(X, Y, test_size=tsize, random_state=rnd_state)

    print('Training Data: ', Xdata.shape)#, ydata.shape)
    print('Testting Data: ', Xtest.shape)#, ytest.shape)
    return [Xdata, ydata, Xtest, ytest]

def run_scenarios(Xdata, ydata,din, default_dict, param_dist):
    '''
    This function initialize the classifier, only RandomForest (by now), then train the model.
    It use RandomSearch strategy to optimize input parameters inside the given intervals.

    input:
        df: is the pandas dataframe with data to be used in training the model
        din: it is a dictionary with values for the model's parameters.
            clf_name = name of the classifier
            n_iter_search =
    output:
        dr : a dataframe with
    '''

    ## build a classifier
    clf_name = din['clf_name'].lower()

    if  clf_name == "randomforest":
        clf = RandomForestClassifier( **default_dict )

    elif clf_name == 'svm':
        clf = SVC( **default_dict )

    # Random_Search
    n_iter_search = din['n_iter_search']
    cv_results, Dt, n_iter_search = random_search(clf_name, clf, param_dist, Xdata, ydata, n_iter_search)

    #Report Results:
    dr = report2(clf_name, cv_results, Dt, n_iter_search, n_top=20)
    print(dr.head(5))
    return dr

def test_models(dr, din, clf_dictdefault, Xdata, ydata, Xtest, ytest):
    ''' Here we test the models agains the test data.
        metrics are defined.
    input:
        dr: dataframe needed to build the model with the optimized set of parameters
        topn:
        clf_dictdefault: it is a dictionary with the default parameter's values.
    output:
    '''
    clf_name = din['clf_name']
    topn = din['n_best']
    # Testing Models:
    ## Get_scenarios:
    my_scenarios = get_best_scenarios(dr, topn, clf_dictdefault)

    # Now testing best scenarios:
    df_output = testing_scenarios(my_scenarios, clf_name, Xdata, ydata, Xtest, ytest)

    for k in din.keys(): df_output['input_'+k] = din[k]

    df_output.to_csv('Results_output.csv',encoding='utf-8', index=False, mode='a', header=True) 
